public class Projectile implements PhysicsObject {
    private double mass; // kg
    private double dragCoefficient;
    private double radius; // m

    public Projectile(double mass, double dragCoefficient, double radius) {
        this.mass = mass;
        this.dragCoefficient = dragCoefficient;
        this.radius = radius;
    }

    @Override
    public double[] calculateDragForce(double[] flowVelocity) {
        double area = Math.PI * Math.pow(radius, 2);
        double flowVelocityMagnitude = Math.sqrt(flowVelocity[0] * flowVelocity[0] + flowVelocity[1] * flowVelocity[1] + flowVelocity[2] * flowVelocity[2]);
        double dragForceMagnitude = -0.5 * dragCoefficient * Constants.DENSITY_AIR * area * flowVelocityMagnitude;

        // Calculate drag force components
        double[] dragForce = {
                dragForceMagnitude * flowVelocity[0] / flowVelocityMagnitude,
                dragForceMagnitude * flowVelocity[1] / flowVelocityMagnitude,
                dragForceMagnitude * flowVelocity[2] / flowVelocityMagnitude
        };

        return dragForce;
    }

    // Getters and setters
    public double getMass() {
        return mass;
    }

    public double getDragCoefficient() {
        return dragCoefficient;
    }

    public double getRadius() {
        return radius;
    }
}
