public class Acceleration {
    private double[] acceleration = new double[3]; // {ax, ay, az} m/s^2


    public Acceleration(double ax, double ay, double az) {
        acceleration[0] = ax;
        acceleration[1] = ay;
        acceleration[2] = az;
    }

    public double getX() {
        return acceleration[0];
    }

    public double getY() {
        return acceleration[1];
    }

    public double getZ() {
        return acceleration[2];
    }

    // Getters and setters
}
