public interface Constants {
    double GRAVITY = 9.81; // m/s^2
    double DENSITY_AIR = 1.225; // kg/m^3 (assumed density of air)
}
