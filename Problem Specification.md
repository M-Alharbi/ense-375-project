## Problem Specification



## Objective

A Howitzer company is asking for a simulator to teach the soldiers about kinematics and dynamics.



## Rigid Body Kinematics and Dynamics

The rigid body kinematics and dynamics of any object expressed in an inertial frame are:

p(t) = v(t)

mv(t) = f(t) + f_d(t) + mg 

In fluid dynamics, the drag force is:

f_d(t) = -0.5C𝜌Au(t)^2




## simulator design test

• Write the simulator using Java programming language. 

• Use GitLab for source code management.

• Choose any function from the simulator and test it with.
  
1. Boundary value testing.
2. Equivalence class testing.
3. Decision table-based testing.
4. Path testing.
5. Dataflow.
6. Slice testing.

• Choose a subset of units to perform Integration testing.
• Choose a subset of use cases to perform System testing.



## Schedule

Due: Day before the final exam at mid night – August 02, 2023 23:59:59


