public class Position {
    private double[] position = new double[2]; // {x, y} meters

    public Position(double x, double y) {
        position[0] = x;
        position[1] = y;
    }

    public double getX() {
        return position[0];
    }

    public double getY() {
        return position[1];
    }

    public void updatePosition(Velocity velocity, double timeStep) {
        position[0] += velocity.getX() * timeStep;
        position[1] += velocity.getY() * timeStep;
    }

    @Override
    public String toString() {
        return "(" + position[0] + ", " + position[1] + ")";
    }
}
