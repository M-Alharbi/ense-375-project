public class Howitzer {
    private double[] barrelPosition = new double[2]; // {x, y} meters
    private double barrelOrientationDegree; // degrees

    public Howitzer(double barrelPositionX, double barrelPositionY, double barrelOrientationDegree) {
        this.barrelPosition[0] = barrelPositionX;
        this.barrelPosition[1] = barrelPositionY;
        this.barrelOrientationDegree = barrelOrientationDegree;
    }

    public double getBarrelPositionX() {
        return barrelPosition[0];
    }

    public double getBarrelPositionY() {
        return barrelPosition[1];
    }

    public double getBarrelOrientationDegree() {
        return barrelOrientationDegree;
    }

    // Getters and setters
}
