import java.util.Arrays;

public class HowitzerSimulation implements PhysicsSimulation {
    private double timeStep; // s
    private double initialSpeed; // m/s
    private double appliedForce; // N
    private double gravity; // m/s^2
    private PhysicsObject projectile;
    private Howitzer howitzer;

    public HowitzerSimulation(Projectile projectile, Howitzer howitzer, double timeStep, double initialSpeed,
                              double appliedForce, double gravity) {
        this.projectile = projectile;
        this.howitzer = howitzer;
        this.timeStep = timeStep;
        this.initialSpeed = initialSpeed;
        this.appliedForce = appliedForce;
        this.gravity = gravity;
    }

    // Method to simulate the howitzer shooting a projectile
    @Override
    public void simulate() {
        // Convert degrees to radians
        double barrelOrientationRad = Math.toRadians(howitzer.getBarrelOrientationDegree());

        // Calculate initial velocity components
        double initialVelocityX = initialSpeed * Math.cos(barrelOrientationRad);
        double initialVelocityY = initialSpeed * Math.sin(barrelOrientationRad);

        // Initialize variables
        Position position = new Position(howitzer.getBarrelPositionX(), howitzer.getBarrelPositionY());
        Velocity velocity = new Velocity(initialVelocityX, initialVelocityY);
        double currentTime = 0.0;

        // Print initial position and velocity
        System.out.println("Initial position: " + position);
        System.out.println("Initial velocity: " + velocity);

        // Perform simulation until the projectile hits the ground (position.y <= 0)
        while (position.getY() > 0.0) {
            // Calculate drag force at time t
            double[] flowVelocity = {velocity.getX() - appliedForce, velocity.getY()}; // Assuming force is applied along the x-axis
            double[] dragForce = projectile.calculateDragForce(flowVelocity);

            // Calculate net force
            double[] netForce = {0.0, -projectile.getMass() * gravity};
            for (int i = 0; i < 2; i++) {
                netForce[i] += dragForce[i];
            }

            // Calculate acceleration
            Acceleration acceleration = new Acceleration(netForce[0] / projectile.getMass(), netForce[1] / projectile.getMass());

            // Update position and velocity using Euler's method
            position.updatePosition(velocity, timeStep);
            velocity.updateVelocity(acceleration, timeStep);

            // Update time
            currentTime += timeStep;

            // Print current position and velocity
            System.out.println("Time: " + currentTime);
            System.out.println("Position: " + position);
            System.out.println("Velocity: " + velocity);
        }

        System.out.println("Projectile hit the ground at time: " + currentTime);
    }
}

