public class Acceleration {
    private double[] acceleration = new double[2]; // {ax, ay} m/s^2

    public Acceleration(double ax, double ay) {
        acceleration[0] = ax;
        acceleration[1] = ay;
    }

    public double getX() {
        return acceleration[0];
    }

    public double getY() {
        return acceleration[1];
    }


}
