public class Main {
    public static void main(String[] args) {
        // Set the parameters for the simulation
        double mass = 10.0; // kg
        double projectileRadius = 0.3; // m
        Projectile projectile = new Projectile(mass, projectileRadius);

        double timeStep = 0.01; // s
        double initialSpeed = 500.0; // m/s
        double appliedForce = 1500.0; // N (force applied along x-axis)
        double gravity = Constants.GRAVITY; // m/s^2

        double barrelPositionX = 0.0; // m
        double barrelPositionY = 30.0; // m
        double barrelOrientationDegree = 30.0; // degrees
        Howitzer howitzer = new Howitzer(barrelPositionX, barrelPositionY, barrelOrientationDegree);

        // Create the simulation object and run the simulation
        PhysicsSimulation simulation = new HowitzerSimulation(projectile, howitzer, timeStep, initialSpeed, appliedForce, gravity);
        simulation.simulate();
    }
}



