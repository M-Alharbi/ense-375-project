public class Velocity {
    private double[] velocity = new double[2]; // {vx, vy} m/s

    public Velocity(double vx, double vy) {
        velocity[0] = vx;
        velocity[1] = vy;
    }

    public double getX() {
        return velocity[0];
    }

    public double getY() {
        return velocity[1];
    }

    public void updateVelocity(Acceleration acceleration, double timeStep) {
        velocity[0] += acceleration.getX() * timeStep;
        velocity[1] += acceleration.getY() * timeStep;
    }

    @Override
    public String toString() {
        return "(" + velocity[0] + ", " + velocity[1] + ")";
    }
}
