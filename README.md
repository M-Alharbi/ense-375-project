# ENSE 375 Project



## Howitzer Firing Simulator

### Introduction
The Howitzer Firing Simulation project aims to develop a simulator to teach soldiers about the principles of kinematics and dynamics involved in firing a Howitzer. The simulator will replicate the process of launching a projectile across an empty field while considering various factors that can affect its trajectory. By accurately representing these instances, soldiers can enhance their understanding and proficiency in using Howitzers for target engagement.

### Problem Definition
The objective of this project is to develop a Howitzer firing simulation program that achieves high accuracy in hitting the target. 
The primary goal is to create a user-friendly application that can be easily operated by soldiers. The program should effectively replicate the process of firing a Howitzer and provide a realistic simulation of the projectile trajectory to ensure accurate target engagement.

### Available Code Versions
Throughout the development process of the Howitzer Firing Simulation, we have iteratively improved and refined the code to achieve better accuracy and user experience. As a result, we offer three different versions of the code, each representing a different stage of development:

1. ##### Version 1:
 Version 1 of the Howitzer Firing Simulation focuses on fundamental projectile motion calculations. Users can explore the effects of barrel pose (position and orientation), projectile radius, mass, initial speed, force applied, and gravity on the trajectory. While this version provides a simple introduction to Howitzer firing, it lacks some advanced features found in later versions. [Link to Version 1 Code](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/version1?ref_type=heads)

2. ##### Version 2:
Version 2 of the Howitzer Firing Simulation introduces several enhancements for a more realistic experience. It includes advanced position and velocity representation, supporting multiple dimensions for physics objects. The calculation of drag force is now incorporated, accounting for air resistance. The updates to position and velocity provide smoother and more accurate trajectory simulation. Soldiers can also control the Howitzer's position and orientation, allowing for precise targeting. [Link to Version 2 Code](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/version2?ref_type=heads)

3. ##### Version 3:
In Version 3 of the Howitzer Firing Simulation, a significant improvement is the addition of a drag coefficient factor. The Projectile class now includes a drag coefficient attribute, allowing for more accurate drag force calculation. The drag force considers flow velocity components (x, y, z), air density, and the projectile's specific drag coefficient, leading to a more realistic trajectory representation. This enhancement enhances the simulation's fidelity and provides soldiers with a more immersive training experience. [Link to Version 3 Code](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/version3?ref_type=heads)

**Notably** Version 3 is the last version in which we conducted extensive testing to ensure the simulator's reliability and correctness.

For a detailed visual representation of the classes and their relationships within the Howitzer Firing Simulation, you can refer to the [Class Diagram](/Documents/class_diagram.pdf) link. The class diagram provides an overview of the program's architecture, showcasing how different classes interact and collaborate to achieve the simulator's functionality.

### Testing
For detailed information about the testing procedures used in the Howitzer Firing Simulation project, please refer to the [TESTING.md](https://gitlab.com/M-Alharbi/ense-375-project/-/blob/main/TESTING.md?ref_type=heads) file. It provides comprehensive insights into the various tests conducted to ensure the accuracy, reliability, and robustness of the simulator. 


### Conclusion
During the development of the Howitzer Firing Simulation, the team encountered and overcame various challenges. These challenges helped us learn valuable lessons and improve our skills. Some key takeaways from the project include:

1. Value of Collaboration: Working as a team allowed us to pool our diverse skills and perspectives, leading to a more robust and efficient simulation program.

2. Adaptation to Challenges: As the project progressed, we encountered unexpected challenges. Our ability to adapt and find innovative solutions was crucial in achieving our objectives.

3. Improved Communication and Group Dynamics: Effective communication and teamwork were vital in ensuring that everyone was on the same page and contributing effectively.

4. Lessons for Future Projects: The experiences gained from this project have provided us with insights and knowledge that we can apply to future simulation and software development endeavors.



### For Future Work
As we continue to refine and enhance the Howitzer Firing Simulation, there are specific areas we plan to focus on:

1. User Input Integration: To increase the simulator's flexibility and interactivity, we aim to implement a user input system. This will allow soldiers to have more control over various firing parameters.

2. Real-Time Visualization: Incorporating real-time visualization of Howitzer firing will further enrich the user experience, making the simulation more engaging and informative.

## Team Members

- Jiabo Zhang 
- Mohammed Alharbi 
- Basheer Ahamed Rafiqudeen




