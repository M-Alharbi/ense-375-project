## Design Requirements



## Objectives

 A Howitzer company have asked us to build a simulator in order to teach soldiers about kinematics and dynamics. The simulator should capture a projectile being shot across an empty field and simulate various instances of the projectile being shot. The possible factors that can affect the instances are the position and the orientation of the barrel pose, the radius and mass of the projectile, drag coefficient, initial speed and force applied to the projectile. The main goal of this project is to design tests for a complete working simulator that could calculate the rigid body kinematics and dynamics of the projectile while simultaneously measuring the drag force endured by it. Functions from the simulator will be subject to various amounts of tests such as Boundary value testing, Equivalence class testing, etc. 	



## Constraints

##### 1. Economic Factors:

1. Cost-effectiveness: cost-effectiveness is crucial in designing the cannon simulator, considering development costs, equipment expenses, and maintenance to stay within budget. Finding the right balance between capabilities and resources ensures that the investment is justified by the benefits and value gained.

2. Lifecycle costs:  Consider lifecycle costs like maintenance, upgrades, and potential obsolescence. A scalable simulator architecture can lower future expenses by enabling simple updates and enhancements, especially when deploying multiple simulators across various training facilities or units.



##### 2. Reliability:


1. System Stability: Emphasize system stability by designing the simulator to be stable and robust. Thorough testing and quality assurance processes should be implemented to identify and resolve any software or hardware issues that could compromise the simulator's reliability.

2. Accuracy and Consistency: Ensure accuracy and consistency by replicating the laws of physics governing projectile motion in the simulator. This provides soldiers with reliable training experiences, builds trust, and enhances their understanding of kinematics and dynamics.



##### 3. Ethics

(1) Integrity: Fabrication and tampering of data and facts are not allowed in simulator design.

(2) Openness: sharing and disclosure of data, results, and resources in the design simulator.

(3) Intellectual property rights: respect patents, copyrights and other forms of intellectual property rights for designing simulators, and plagiarism is not allowed.



##### 4. Sustainability and Environmental Considerations

(1) Effectiveness: The software system can make the most effective use of the time and space resources of the computer.

(2) Maintainability: After the software is delivered for use, it can be modified to correct latent errors, improve performance and other attributes, and adapt the software product to changes in the environment.
