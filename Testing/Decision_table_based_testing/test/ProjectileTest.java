import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ProjectileTest {

    @Test
    public void testCalculateDragForceCase1() {
        double flowVelocityX = 10.0;
        double flowVelocityY = 0.0;
        double flowVelocityZ = 0.0;
        double expectedDragForceX = -0.008;
        double expectedDragForceY = 0.0;
        double expectedDragForceZ = 0.0;

        Projectile projectile = new Projectile(10.0, 0.08, 0.3);
        double[] flowVelocity = {flowVelocityX, flowVelocityY, flowVelocityZ};
        double[] dragForce = projectile.calculateDragForce(flowVelocity);

        assertEquals(expectedDragForceX, dragForce[0], 0.0001);
        assertEquals(expectedDragForceY, dragForce[1], 0.0001);
        assertEquals(expectedDragForceZ, dragForce[2], 0.0001);
    }

    @Test
    public void testCalculateDragForceCase2() {
        double flowVelocityX = 0.0;
        double flowVelocityY = 5.0;
        double flowVelocityZ = 5.0;
        double expectedDragForceX = 0.0;
        double expectedDragForceY = -0.08;
        double expectedDragForceZ = -0.08;

        Projectile projectile = new Projectile(10.0, 0.08, 0.3);
        double[] flowVelocity = {flowVelocityX, flowVelocityY, flowVelocityZ};
        double[] dragForce = projectile.calculateDragForce(flowVelocity);

        assertEquals(expectedDragForceX, dragForce[0], 0.0001);
        assertEquals(expectedDragForceY, dragForce[1], 0.0001);
        assertEquals(expectedDragForceZ, dragForce[2], 0.0001);
    }

    @Test
    public void testCalculateDragForceCase3() {
        double flowVelocityX = 2.0;
        double flowVelocityY = 2.0;
        double flowVelocityZ = 2.0;
        double expectedDragForceX = -0.002256;
        double expectedDragForceY = -0.002256;
        double expectedDragForceZ = -0.002256;

        Projectile projectile = new Projectile(10.0, 0.08, 0.3);
        double[] flowVelocity = {flowVelocityX, flowVelocityY, flowVelocityZ};
        double[] dragForce = projectile.calculateDragForce(flowVelocity);

        assertEquals(expectedDragForceX, dragForce[0], 0.0001);
        assertEquals(expectedDragForceY, dragForce[1], 0.0001);
        assertEquals(expectedDragForceZ, dragForce[2], 0.0001);
    }

    @Test
    public void testCalculateDragForceCase4() {
        double flowVelocityX = 0.0;
        double flowVelocityY = 0.0;
        double flowVelocityZ = 0.0;
        double expectedDragForceX = 0.0;
        double expectedDragForceY = 0.0;
        double expectedDragForceZ = 0.0;

        Projectile projectile = new Projectile(10.0, 0.08, 0.3);
        double[] flowVelocity = {flowVelocityX, flowVelocityY, flowVelocityZ};
        double[] dragForce = projectile.calculateDragForce(flowVelocity);

        assertEquals(expectedDragForceX, dragForce[0], 0.0001);
        assertEquals(expectedDragForceY, dragForce[1], 0.0001);
        assertEquals(expectedDragForceZ, dragForce[2], 0.0001);
    }

    @Test
    public void testCalculateDragForceCase5() {
        double flowVelocityX = 0.0;
        double flowVelocityY = 10.0;
        double flowVelocityZ = 10.0;
        double expectedDragForceX = 0.0;
        double expectedDragForceY = -0.16;
        double expectedDragForceZ = -0.16;

        Projectile projectile = new Projectile(10.0, 0.08, 0.3);
        double[] flowVelocity = {flowVelocityX, flowVelocityY, flowVelocityZ};
        double[] dragForce = projectile.calculateDragForce(flowVelocity);

        assertEquals(expectedDragForceX, dragForce[0], 0.0001);
        assertEquals(expectedDragForceY, dragForce[1], 0.0001);
        assertEquals(expectedDragForceZ, dragForce[2], 0.0001);
    }
}
