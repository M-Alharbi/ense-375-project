package version3;

public interface PhysicsObject {
    double[] calculateDragForce(double[] flowVelocity);

    double getMass();
}
