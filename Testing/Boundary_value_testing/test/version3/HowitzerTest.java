package version3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HowitzerTest {

    @Test
    public void testBarrelPositionZWithLowerBoundary() {
        double barrelPositionZ = Double.MIN_VALUE;

        Howitzer howitzer = new Howitzer(0.0, 0.0, barrelPositionZ, 30.0);

        assertEquals(barrelPositionZ, howitzer.getBarrelPositionZ(), 0.0001);
    }

    @Test
    public void testBarrelPositionZWithUpperBoundary() {
        double barrelPositionZ = Double.MAX_VALUE;

        Howitzer howitzer = new Howitzer(0.0, 0.0, barrelPositionZ, 30.0);

        assertEquals(barrelPositionZ, howitzer.getBarrelPositionZ(), 0.0001);
    }

    @Test
    public void testBarrelPositionZWithPositiveValue() {
        double barrelPositionZ = 10.0;

        Howitzer howitzer = new Howitzer(0.0, 0.0, barrelPositionZ, 30.0);

        assertEquals(barrelPositionZ, howitzer.getBarrelPositionZ(), 0.0001);
    }

    @Test
    public void testBarrelPositionZWithNegativeValue() {
        double barrelPositionZ = -10.0;

        Howitzer howitzer = new Howitzer(0.0, 0.0, barrelPositionZ, 30.0);

        assertEquals(barrelPositionZ, howitzer.getBarrelPositionZ(), 0.0001);
    }
}

