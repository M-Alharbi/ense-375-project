package version3;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProjectileTest {

    @Test
    public void testDragCoefficientWithLowerBoundary() {
        double dragCoefficient = Double.MIN_VALUE;

        Projectile projectile = new Projectile(10.0, dragCoefficient, 0.3);

        assertEquals(dragCoefficient, projectile.getDragCoefficient(), 0.0001);
    }

    @Test
    public void testDragCoefficientWithUpperBoundary() {
        double dragCoefficient = Double.MAX_VALUE;

        Projectile projectile = new Projectile(10.0, dragCoefficient, 0.3);

        assertEquals(dragCoefficient, projectile.getDragCoefficient(), 0.0001);
    }

    @Test
    public void testDragCoefficientWithPositiveValue() {
        double dragCoefficient = 0.08;

        Projectile projectile = new Projectile(10.0, dragCoefficient, 0.3);

        assertEquals(dragCoefficient, projectile.getDragCoefficient(), 0.0001);
    }

    @Test
    public void testDragCoefficientWithNegativeValue() {
        double dragCoefficient = -0.08;

        Projectile projectile = new Projectile(10.0, dragCoefficient, 0.3);

        assertEquals(dragCoefficient, projectile.getDragCoefficient(), 0.0001);
    }
}

