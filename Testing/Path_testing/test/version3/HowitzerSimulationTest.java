package version3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HowitzerSimulationTest {

    @Test
    public void testProjectileAboveGround() {
        // Test Case 1: Projectile is above the ground (position.getZ() > 0.0)
        double mass = 10.0; // kg
        double dragCoefficient = 0.08;
        double projectileRadius = 0.3; // m
        Projectile projectile = new Projectile(mass, dragCoefficient, projectileRadius);

        double timeStep = 0.01; // s
        double initialSpeed = 500.0; // m/s
        double appliedForce = 1500.0; // N (force applied along x-axis)
        double gravity = Constants.GRAVITY; // m/s^2

        double barrelPositionX = 0.0; // m
        double barrelPositionY = 0.0; // m
        double barrelPositionZ = 10.0; // m (initial height above the ground)
        double barrelOrientationDegree = 30.0; // degrees
        Howitzer howitzer = new Howitzer(barrelPositionX, barrelPositionY, barrelPositionZ, barrelOrientationDegree);

        HowitzerSimulation simulation = new HowitzerSimulation(projectile, howitzer, timeStep, initialSpeed, appliedForce, gravity);
        simulation.simulate();
        
    }

    @Test
    public void testProjectileHitsGround() {
        // Test Case 2: Projectile hits the ground (position.getZ() <= 0.0)
        double mass = 10.0; // kg
        double dragCoefficient = 0.08;
        double projectileRadius = 0.3; // m
        Projectile projectile = new Projectile(mass, dragCoefficient, projectileRadius);

        double timeStep = 0.01; // s
        double initialSpeed = 500.0; // m/s
        double appliedForce = 1500.0; // N (force applied along x-axis)
        double gravity = Constants.GRAVITY; // m/s^2

        double barrelPositionX = 0.0; // m
        double barrelPositionY = 0.0; // m
        double barrelPositionZ = -1.0; // m (initial height below the ground)
        double barrelOrientationDegree = 30.0; // degrees
        Howitzer howitzer = new Howitzer(barrelPositionX, barrelPositionY, barrelPositionZ, barrelOrientationDegree);

        HowitzerSimulation simulation = new HowitzerSimulation(projectile, howitzer, timeStep, initialSpeed, appliedForce, gravity);
        simulation.simulate();
        
    }

    @Test
    public void testZeroDragCoefficient() {
        // Test Case 3: Projectile's drag coefficient is zero (dragCoefficient = 0)
        double mass = 10.0; // kg
        double dragCoefficient = 0.0; // Zero drag coefficient
        double projectileRadius = 0.3; // m
        Projectile projectile = new Projectile(mass, dragCoefficient, projectileRadius);

        double timeStep = 0.01; // s
        double initialSpeed = 500.0; // m/s
        double appliedForce = 1500.0; // N (force applied along x-axis)
        double gravity = Constants.GRAVITY; // m/s^2

        double barrelPositionX = 0.0; // m
        double barrelPositionY = 0.0; // m
        double barrelPositionZ = 10.0; // m (initial height above the ground)
        double barrelOrientationDegree = 30.0; // degrees
        Howitzer howitzer = new Howitzer(barrelPositionX, barrelPositionY, barrelPositionZ, barrelOrientationDegree);

        HowitzerSimulation simulation = new HowitzerSimulation(projectile, howitzer, timeStep, initialSpeed, appliedForce, gravity);
        simulation.simulate();
        
    }
}

