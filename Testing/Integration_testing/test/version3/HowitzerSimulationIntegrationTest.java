package version3;
import static org.junit.Assert.*;
import org.junit.Test;

public class HowitzerSimulationIntegrationTest {

    @Test
    public void testHowitzerSimulationIntegration() {
        // Set up a valid Projectile object
        double mass = 10.0; // kg
        double dragCoefficient = 0.08;
        double projectileRadius = 0.3; // m
        Projectile projectile = new Projectile(mass, dragCoefficient, projectileRadius);

        // Set up a Howitzer object
        double barrelPositionX = 0.0; // m
        double barrelPositionY = 0.0; // m
        double barrelPositionZ = 10.0; // m (initial height above the ground)
        double barrelOrientationDegree = 30.0; // degrees
        Howitzer howitzer = new Howitzer(barrelPositionX, barrelPositionY, barrelPositionZ, barrelOrientationDegree);

        // Set up parameters for the simulation
        double timeStep = 0.01; // s
        double initialSpeed = 500.0; // m/s
        double appliedForce = 1500.0; // N (force applied along x-axis)
        double gravity = Constants.GRAVITY; // m/s^2

        // Create the HowitzerSimulation object
        HowitzerSimulation simulation = new HowitzerSimulation(projectile, howitzer, timeStep, initialSpeed, appliedForce, gravity);

        // Run the simulation and check for exceptions
        try {
            simulation.simulate();
        } catch (Exception e) {
            fail("Simulation should not throw any exceptions.");
        }

        // If the simulation runs without exceptions, it's considered successful
        assertTrue(true);
    }
}
