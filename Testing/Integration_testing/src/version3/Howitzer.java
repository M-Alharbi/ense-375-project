package version3;

public class Howitzer {
    private double[] barrelPosition = new double[3]; // {x, y, z} meters
    private double barrelOrientationDegree; // degrees

    public Howitzer(double barrelPositionX, double barrelPositionY, double barrelPositionZ, double barrelOrientationDegree) {
        this.barrelPosition[0] = barrelPositionX;
        this.barrelPosition[1] = barrelPositionY;
        this.barrelPosition[2] = barrelPositionZ;
        this.barrelOrientationDegree = barrelOrientationDegree;
    }

    public double getBarrelPositionX() {
        return barrelPosition[0];
    }

    public double getBarrelPositionY() {
        return barrelPosition[1];
    }

    public double getBarrelPositionZ() {
        return barrelPosition[2];
    }

    public double getBarrelOrientationDegree() {
        return barrelOrientationDegree;
    }

    // Getters and setters 
}
