#### Path testing

In these test classes, we have identify three potential paths:

1. Path when the projectile is above the ground (position.getZ() > 0.0):
This path involves the while loop where the simulation continues until the projectile hits the ground.

2. Path when the projectile hits the ground (position.getZ() <= 0.0):
This path involves the termination of the while loop when the projectile's Z position becomes less than or equal to 0.

3. Path when the projectile's drag coefficient is zero (dragCoefficient = 0):
This path involves the absence of any drag force on the projectile.

[Link for path testing](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/Testing/Path_testing?ref_type=heads)


#### Boundary testing
In these test classes, we have four test cases each:
1. `testBarrelPositionZWithLowerBoundary` and `testDragCoefficientWithLowerBoundary`: These test the lower boundary values for `barrelPositionZ` and `dragCoefficient` respectively.
2. `testBarrelPositionZWithUpperBoundary` and `testDragCoefficientWithUpperBoundary`: These test the upper boundary values for `barrelPositionZ` and `dragCoefficient` respectively.
3. `testBarrelPositionZWithPositiveValue` and `testDragCoefficientWithPositiveValue`: These test positive values for `barrelPositionZ` and `dragCoefficient` respectively.
4. `testBarrelPositionZWithNegativeValue` and `testDragCoefficientWithNegativeValue`: These test negative values for `barrelPositionZ` and `dragCoefficient` respectively.

[Link for Boundary testing](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/Testing/Boundary_value_testing?ref_type=heads)


#### Decision table based testing

The 'ProjectileTest' class contains five JUnit test methods, each representing a different scenario for the 'calculateDragForce' method in the 'Projectile' class. The tests set specific flow velocity values, calculate the expected drag force components, and compare the actual results with the expected values.

[Link for Decision table based testing](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/Testing/Decision_table_based_testing?ref_type=heads)


#### Data flow testing

We did data flow testing to ensure the data flow is examined with respect to the variables used in the code. We didnt use du pair coverage since we had a lot of definitions in the HowitzerSimulation so we created a data flow testing diagram which we have uploaded in the project Repository. 

![Data_flow_testing](/Documents/Data_flow_Testing.png)


#### Integration testing

Our integration tests test the interaction between the "HowitzerSimulation" and "Projectile" classes. By creating a test class for integration testing. Let's call it "HowitzerSimulationIntegrationTest". In this test class, create a test method to simulate the shooting process and verify the results. Then set a "Projectile" object with valid parameters and a "HowitzerSimulation" object in the test method. Then run the simulation by calling the "simulate()" method on the "HowitzerSimulation" object. Finally verify the results and make sure the simulation runs without any anomalies.

[Link for Integration testing](https://gitlab.com/M-Alharbi/ense-375-project/-/tree/main/Testing/Integration_testing?ref_type=heads)


#### System Testing

System testing examines every component of an application to make sure that they work as a complete and unified whole. We tested all three versions we had and we created a rough Finite State Machine of our project where we tested how HowitzerSimulation worked. 


![System_Testing](/Documents/System_Testing.png)
