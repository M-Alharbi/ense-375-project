public interface PhysicsObject {
    double[] calculateDragForce(double[] flowVelocity);

    double getMass();
}
