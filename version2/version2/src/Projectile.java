public class Projectile implements PhysicsObject {
    private double mass; // kg
    private double radius; // m

    public Projectile(double mass, double radius) {
        this.mass = mass;
        this.radius = radius;
    }

    @Override
    public double[] calculateDragForce(double[] flowVelocity) {
        double area = Math.PI * Math.pow(radius, 2);
        double flowVelocityMagnitude = Math.sqrt(flowVelocity[0] * flowVelocity[0] + flowVelocity[1] * flowVelocity[1] + flowVelocity[2] * flowVelocity[2]);
        double dragForceMagnitude = -0.5 * Constants.DENSITY_AIR * area * flowVelocityMagnitude;

        // Calculate drag force components
        double[] dragForce = {
                dragForceMagnitude * flowVelocity[0] / flowVelocityMagnitude,
                dragForceMagnitude * flowVelocity[1] / flowVelocityMagnitude,
                dragForceMagnitude * flowVelocity[2] / flowVelocityMagnitude
        };

        return dragForce;
    }

    // Getters and setters (if needed)
    public double getMass() {
        return mass;
    }

    public double getRadius() {
        return radius;
    }
}

