public class Velocity {
    private double[] velocity = new double[3]; // {vx, vy, vz} m/s

    public Velocity(double vx, double vy, double vz) {
        velocity[0] = vx;
        velocity[1] = vy;
        velocity[2] = vz;
    }

    public double getX() {
        return velocity[0];
    }

    public double getY() {
        return velocity[1];
    }

    public double getZ() {
        return velocity[2];
    }

    public void updateVelocity(Acceleration acceleration, double timeStep) {
        velocity[0] += acceleration.getX() * timeStep;
        velocity[1] += acceleration.getY() * timeStep;
        velocity[2] += acceleration.getZ() * timeStep;
    }

    @Override
    public String toString() {
        return "(" + velocity[0] + ", " + velocity[1] + ", " + velocity[2] + ")";
    }
}
